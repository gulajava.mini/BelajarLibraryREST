package gulajava.teslibraryrests.internet.okhttps;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class ApisOkHttp {


    /**
     * API UNTUK DAFTAR KANDIDAT
     **/
    //http://api.pemiluapi.org/calonpilkada/api/candidates?apiKey=8e67d2396454f98c370596b139194015&limit=100
    public static String getKandidatDaftar(String Apikey, String limit) {

        return "/calonpilkada/api/candidates?apiKey=" + Apikey + "&limit=" + limit;
    }

}
