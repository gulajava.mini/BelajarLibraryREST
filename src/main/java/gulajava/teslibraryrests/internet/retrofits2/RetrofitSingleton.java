package gulajava.teslibraryrests.internet.retrofits2;

import com.fasterxml.jackson.databind.ObjectMapper;

import gulajava.teslibraryrests.internet.KonstanInternet;
import gulajava.teslibraryrests.internet.OkHttpKlienSingleton;
import gulajava.teslibraryrests.internet.konverters.JacksonObjectMapper;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by Gulajava Ministudio on 2/8/16.
 */
public class RetrofitSingleton {

    private static RetrofitSingleton ourInstance;

    private Retrofit mRetrofit;
    private ApisRetro2 mApisRetro2 = null;


    public static RetrofitSingleton getInstance() {

        if (ourInstance == null) {
            ourInstance = new RetrofitSingleton();
        }
        return ourInstance;
    }

    private RetrofitSingleton() {

        OkHttpClient okHttpClient = OkHttpKlienSingleton.getInstance().getOkHttpClient();
        ObjectMapper objectMapper = JacksonObjectMapper.getInstance().getObjectMapper();

        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(KonstanInternet.ALAMATSERVER);
        builder.client(okHttpClient);
        builder.addConverterFactory(JacksonConverterFactory.create(objectMapper));

        mRetrofit = builder.build();
    }


    public ApisRetro2 getApisRetro2() {

        if (mApisRetro2 == null) {
            mApisRetro2 = mRetrofit.create(ApisRetro2.class);
        }

        return mApisRetro2;
    }
}
