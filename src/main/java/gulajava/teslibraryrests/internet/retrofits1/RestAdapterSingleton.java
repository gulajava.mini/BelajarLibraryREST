package gulajava.teslibraryrests.internet.retrofits1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jakewharton.retrofit.Ok3Client;

import gulajava.teslibraryrests.internet.KonstanInternet;
import gulajava.teslibraryrests.internet.OkHttpKlienSingleton;
import gulajava.teslibraryrests.internet.konverters.JacksonObjectMapper;
import okhttp3.OkHttpClient;
import retrofit.RestAdapter;
import retrofit.converter.JacksonConverter;

/**
 * Created by Gulajava Ministudio on 2/1/16.
 */
public class RestAdapterSingleton {

    private static RestAdapterSingleton ourInstance;
    private RestAdapter mRestAdapter;
    private Apis mApis;

    public static RestAdapterSingleton getInstance() {

        if (ourInstance == null) {
            ourInstance = new RestAdapterSingleton();
        }

        return ourInstance;
    }

    private RestAdapterSingleton() {

        OkHttpClient okHttpClient = OkHttpKlienSingleton.getInstance().getOkHttpClient();
        ObjectMapper objectMapper = JacksonObjectMapper.getInstance().getObjectMapper();

        mRestAdapter = new RestAdapter.Builder()
                .setEndpoint(KonstanInternet.ALAMATSERVER)
                .setClient(new Ok3Client(okHttpClient))
                .setConverter(new JacksonConverter(objectMapper))
                .build();

    }

    public Apis getApis() {

        if (mApis == null) {
            mApis = mRestAdapter.create(Apis.class);
        }
        return mApis;
    }
}
