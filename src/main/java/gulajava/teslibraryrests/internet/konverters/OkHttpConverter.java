package gulajava.teslibraryrests.internet.konverters;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import gulajava.teslibraryrests.modelan.Candidates;
import gulajava.teslibraryrests.modelan.DataResults;
import gulajava.teslibraryrests.modelan.KandidatModel;
import gulajava.teslibraryrests.modelan.Results;
import okhttp3.Response;

/**
 * Created by Gulajava Ministudio on 2/2/16.
 */
public class OkHttpConverter {


    public OkHttpConverter() {
    }


    public static List<Candidates> getCandidatParse(Response response) {

        List<Candidates> candidatesList = new ArrayList<>();

        ObjectMapper objectMapper = JacksonObjectMapper.getInstance().getObjectMapper();

        try {
            KandidatModel kandidatModel = objectMapper.readValue(response.body().byteStream(), KandidatModel.class);
            DataResults dataResults = kandidatModel.getData();
            Results results = dataResults.getResults();
            candidatesList = results.getCandidates();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return candidatesList;
    }


    public static List<Candidates> getCandidatParseString(String response) {

        List<Candidates> candidatesList = new ArrayList<>();

        ObjectMapper objectMapper = JacksonObjectMapper.getInstance().getObjectMapper();

        try {
            KandidatModel kandidatModel = objectMapper.readValue(response, KandidatModel.class);
            DataResults dataResults = kandidatModel.getData();
            Results results = dataResults.getResults();
            candidatesList = results.getCandidates();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return candidatesList;
    }

}
