package gulajava.teslibraryrests.internet;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Gulajava Ministudio on 2/1/16.
 */
public class OkHttpKlienSingleton {

    private static OkHttpKlienSingleton ourInstance;

    private OkHttpClient mOkHttpClient;

    public static OkHttpKlienSingleton getInstance() {

        if (ourInstance == null) {
            ourInstance = new OkHttpKlienSingleton();
        }

        return ourInstance;
    }

    private OkHttpKlienSingleton() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(10000, TimeUnit.MILLISECONDS);
        builder.readTimeout(10000, TimeUnit.MILLISECONDS);
        builder.writeTimeout(10000, TimeUnit.MILLISECONDS);
        builder.addInterceptor(getInterceptor());

        mOkHttpClient = builder.build();
        setOkHttpClient(mOkHttpClient);
    }


    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    public void setOkHttpClient(OkHttpClient okHttpClient) {
        mOkHttpClient = okHttpClient;
    }

    private Interceptor getInterceptor() {

        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request newRequest = chain.request().newBuilder()
                        .addHeader("Accept", "application/json")
                        .build();

                return chain.proceed(newRequest);
            }
        };
    }
}
